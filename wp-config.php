<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'fma_wordpress');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&w(9.@-X-c~Jy-<MP.@pPPIBCOI3WYLrR1o^pM:)>_=lF`@MuXg9>%(seqm?>1k[');
define('SECURE_AUTH_KEY',  'Z`?vA}WNP{?Z]|Ng;NVHD6xI(kKh2b)Jg`rpkYd_M-F]-w$tByu/uSA81:$~fKD:');
define('LOGGED_IN_KEY',    'r=hn<D4fS]/!Xxal}hJowMEp}w?EJcG~tzTgZ{,JL@z#17~#f~Q@MD6Ab </Gs@d');
define('NONCE_KEY',        'WbB~HA^YcU//W|{hvL%u}AqdA}73.(!megnsJli-leo~.:[;~[ghHo0r<^ [1zr|');
define('AUTH_SALT',        '/xpXlnx$D[(J7VBjDS)+(*tLA!kNazz8<Lm@$Xn74^+6tl!%dlM=+4iw?l@E[#32');
define('SECURE_AUTH_SALT', 'sp!jc,B?9&U7Aef9a5:KsKpEZz+oN2v@9krO,*ovTf&:EPkW)FMm^1 %gO:WB4Um');
define('LOGGED_IN_SALT',   '/P<928+;WtbHsszB#joZ{MayfQ#3+oQ?V:Y-X,Hd<a`{AE`: Xd%8o+y727{0oLT');
define('NONCE_SALT',       '64rbt#RSZ)BDR,!m>5wV)Y~?r)t0gXsCovaDn^Vq7C(@pi%=^$)MSHM&!OZ84uT-');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
