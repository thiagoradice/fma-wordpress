<?php 
	get_header(); 
	
	$post_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID(),"large"));
	
	echo "<div class='post-capa'><img src='".$post_image."' /></div>"
?>
	<div class="d-flex justify-content-between conteudo">
		<div id="primary" class="content-area col-8 p-0 pr-3">
			<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();
					
					$out = "<div class='fma-post-single'>";
					$out .= "<div class='text d-flex flex-column justify-content-between'>";
					$out .= "<h2 class='title'>".get_the_title()."</h2>";
					$out .= "<p>".get_the_date()."</p>";
					$out .= "<div class='author'>Escrito por <b>".get_the_author()."</b></div>";
					$out .= "<div class='post'>".nl2br(get_the_content())."</div>";
					$out .= "</div>";
					$out .= "</div>";
					
					echo $out;
				endwhile;

				// Previous/next page navigation.
				the_posts_pagination();

			// If no content, include the "No posts found" template.
			else :
				echo "Nenhum post encontrado.";

			endif;
			?>
			<div class="section-fma mt-5 mb-4">Outras postagens de <?php echo get_the_author() ?></div>
			<div class="author-posts">
				<?php 
					$author_posts = get_posts(array(
										'author'        =>  get_the_author_meta('ID'),
										'orderby'       =>  'post_date',
										'order'         =>  'DESC',
										'posts_per_page' => 2
									));
									
					foreach($author_posts as $ap){
						$post_image = wp_get_attachment_url(get_post_thumbnail_id($ap->ID));
						
						$out  = "<a href='".get_the_permalink($ap->ID)."' >";
						$out .= "<div class='fma-blog-post d-flex align-items-stretch'>";
						$out .= $post_image ? "<div class='thumbnail col-6 p-0 mr-3' style='background-image:url(".$post_image.")' /></div>" : "";
						$out .= "<div class='text d-flex flex-column justify-content-between'>";
						$out .= "<h2 class='title'>".get_the_title($ap->ID)."</h2>";
						$out .= "<div class='excerpt'>".get_the_excerpt($ap->ID)."</div>";
						$out .= "<div class='author'>Escrito por <b>".get_the_author($ap->ID)."</b></div>";
						$out .= "</div>";
						$out .= "</div>";
						$out .= "</a>";
						
						echo $out;
					}
				?>
			</div>
			<div class="section-fma mt-5 mb-4">Comentários</div>
			<?php echo comments_template('', true); ?>
			</main><!-- .site-main -->
		</div><!-- .content-area -->
		
		<div class="col-4 p-0">
			<div class="widget-newsletter fma-boxes-home mt-0">
				<div class="newsletter py-3 px-4">
					<h3 class="my-0 px-3">Receba nosso conteúdo em primeira mão!</h3>
					<?php es_subbox($namefield = "NO", $desc = "", $group = "Public"); ?>
				</div>			
			</div>
			
			<div class="widget-autores fma-boxes-home mt-0">
				<div class="section-fma">Outros colunistas</div>
				<div class="autores">
					<div class='autores-nav'>
						<a href='#prev'><span class='nav-button pl-0'><i class='fa fa-chevron-left mr-auto'></i></span></a>
						<a href='#next'><span class='nav-button pr-0'><i class='fa fa-chevron-right ml-auto'></i></span></a>
					</div>
					
					<div class="wrapper"><?php fma_get_authors(); ?></div>
				</div>
			</div>
			
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
