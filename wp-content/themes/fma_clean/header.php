<?php
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<div class="barra-topo d-flex d-md-none justify-content-between align-items-center">	
		<a href="#" class="open-menu">
			<i class="fa fa-close"></i>
			<i class="fa fa-bars"></i>
		</a>
		<?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>
			<nav id="social-navigation" class="social-navigation ml-auto" role="navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'social',
						'menu_class'     => 'social-links-menu d-flex',
						'depth'          => 1
					) );
				?>
			</nav><!-- .social-navigation -->
		<?php endif; ?>
		
		<?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>
			<div id="site-header-menu" class="site-header-menu menu-mobile">
				<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<nav id="site-navigation" class="main-navigation" role="navigation">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class'     => 'd-flex justify-content-between col-12',
							 ) );
						?>
					</nav><!-- .main-navigation -->
				<?php endif; ?>
			</div><!-- .site-header-menu -->
		<?php endif; ?>
	</div>
	
<div id="page" class="site d-flex justify-content-center">
	<div class="site-inner">
		<header id="masthead" class="site-header d-flex justify-content-between" role="banner">
			<div class="site-branding mb-5">
				<?php fma_the_custom_logo(); ?>
			</div>
			
			<div class="search-social mt-auto d-flex flex-column">
				<?php $http = $_SERVER['HTTPS'] ? "https://" : "http://"; ?>
				<form role="search" method="get" class="search-form d-flex justify-content-stretch" action="<?php echo $http.$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"] ?>">
					<input type="search" class="search-field" placeholder="O que você quer buscar?" value="<?php echo $s ?>" name="s">
					<button type="submit" class="search-submit"><i class='fa fa-search'></i></button>
				</form>

				<?php if ( has_nav_menu( 'social' ) ) : ?>
					<nav id="social-navigation" class="social-navigation ml-auto d-none d-md-block" role="navigation">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu d-flex',
								'depth'          => 1
							) );
						?>
					</nav><!-- .social-navigation -->
				<?php endif; ?>
			</div><!-- .site-header-main -->
		</header><!-- .site-header -->
				
		<?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>
			<div id="site-header-menu" class="site-header-menu d-none d-md-block">
				<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<nav id="site-navigation" class="main-navigation" role="navigation">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class'     => 'd-flex justify-content-between col-12',
							 ) );
						?>
					</nav><!-- .main-navigation -->
				<?php endif; ?>
			</div><!-- .site-header-menu -->
		<?php endif; ?>

		<div id="content" class="site-content">
