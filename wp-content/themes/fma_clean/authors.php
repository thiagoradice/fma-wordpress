<?php /* Template Name: Colunistas */ ?>

<?php get_header(); ?>
	<div class="d-flex justify-content-between conteudo mt-3">
		<div id="primary" class="content-area col-8 p-0 pr-3">
			<main id="main" class="site-main" role="main">
				<div class="section-fma">Colunistas</div>
				
				 <?php 
					$authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users ORDER BY display_name");
					
					foreach($authors as $author){
				?> 

				<div class="d-flex mb-5 autor">
					<div id="author-avatar" class="mr-3">
						<?php echo get_avatar($author->ID, "thumbnail" ); ?>
					</div><!-- #author-avatar -->
					
					<div id="author-description">
						<h2><?php echo the_author_meta('display_name', $author->ID); ?></h2>
						<?php the_author_meta( 'description',$author->ID ); ?>
						<div class="mt-3"><a href="<?php echo get_bloginfo('url')."/?author=".$author->ID;?>" class="button">Ver artigos</a></div>
					</div><!-- #author-description -->
				</div><!-- #entry-author-info -->
				
				<?php } ?>
			</main><!-- .site-main -->
		</div><!-- .content-area -->
		
		<div class="col-4 p-0">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
