<?php 
	get_header(); 
	
	$post_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID(),"large"));
	
	echo "<div class='post-capa'><img src='".$post_image."' /></div>";
?>
	<div class="d-flex justify-content-between conteudo">
		<div id="primary" class="content-area col-8 p-0 pr-3">
			<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();
					
					$out = "<div class='fma-post-single'>";
					$out .= "<div class='text  keep-normal d-flex flex-column justify-content-between'>";
					$out .= "<div class='section-fma'>".get_the_title()."</div>";
					$out .= "<div class='post'>".get_the_content()."</div>";
					$out .= "</div>";
					$out .= "</div>";
					
					echo $out;
				endwhile;

				// Previous/next page navigation.
				the_posts_pagination();

			// If no content, include the "No posts found" template.
			else :
				echo "Nenhum post encontrado.";

			endif;
			
			echo do_shortcode('[contact-form-7 id="101" title="Formulário de contato 1"]');
			?>
			</main><!-- .site-main -->
		</div><!-- .content-area -->
		
		<div class="col-4 p-0">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
