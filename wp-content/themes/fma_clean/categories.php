<?php /* Template Name: Assuntos */ ?>

<?php get_header(); ?>
	<div class="d-flex justify-content-between conteudo mt-3">
		<div id="primary" class="content-area col-8 p-0 pr-3">
			<main id="main" class="site-main" role="main">
				<?php if ( have_posts() ) : ?>
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();
						
						$out = "<div class='fma-post-single'>";
						$out .= "<div class='text keep-normal d-flex flex-column justify-content-between'>";
						$out .= "<div class='section-fma'>".get_the_title()."</div>";
						$out .= "<div class='post'>".get_the_content()."</div>";
						$out .= "</div>";
						$out .= "</div>";
						
						echo $out;
					endwhile;

					// Previous/next page navigation.
					the_posts_pagination();

				// If no content, include the "No posts found" template.
				else :
					echo "Nenhum post encontrado.";

				endif;
				?>
				
				<ul class="fma-assuntos">
				 <?php 
					$assuntos = get_categories(array(
						"hide_empty" => false
					));
					
					foreach($assuntos as $assunto){
				?> 
					<li>
						<h2><?php echo $assunto->name ?></h2>
						<?php echo $assunto->description ?>
						<div class="mt-3"><a href="<?php echo get_category_link($assunto->term_id) ?>" class="button"><?php echo $assunto->category_count ?> posts</a></div>
					</li>
				<?php } ?>
				</ul>
			</main><!-- .site-main -->
		</div><!-- .content-area -->
		
		<div class="col-4 p-0">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
