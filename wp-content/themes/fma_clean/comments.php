<div id="comments" class="comments-area">

	<?php
		comment_form( array(
			'comment_field' =>  '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) .
			'</label><textarea id="comment" name="comment" cols="45" rows="8" placeholder="Escreva aqui seu comentário" aria-required="true">' .
			'</textarea></p>',
			'label_submit'=>'Enviar',
		) );
	?>

	<?php if ( have_comments() ) : ?>
		<?php the_comments_navigation(); ?>

		<div class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'div',
					'short_ping'  => true,
					'avatar_size' => 0,
				) );
			?>
		</div><!-- .comment-list -->

		<?php the_comments_navigation(); ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentysixteen' ); ?></p>
	<?php endif; ?>

</div><!-- .comments-area -->
