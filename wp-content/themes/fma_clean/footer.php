		</div><!-- .site-content -->
	</div><!-- .site-inner -->
</div><!-- .site -->

	<footer id="colophon" class="site-footer d-flex flex-column justify-content-center align-items-center" role="contentinfo">
		<div class="links-footer d-flex justify-content-between">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation"">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'text-center',
							'items_wrap'      => '<ul class="%2$s"><li id="item-id" class="title">Menu</li>%3$s</ul>'
						 ) );
					?>
				</nav><!-- .main-navigation -->
				<nav class="main-navigation" role="navigation"">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'assuntos',
							'menu_class'     => 'text-center',
							'items_wrap'      => '<ul class="%2$s"><li id="item-id" class="title">Assuntos</li>%3$s</ul>'
						 ) );
					?>
				</nav><!-- .main-navigation -->
				<nav class="main-navigation" role="navigation"">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'partners',
							'menu_class'     => 'text-center',
							'items_wrap'      => '<ul class="%2$s"><li id="item-id" class="title">Parceiros</li>%3$s</ul>'
						 ) );
					?>
				</nav><!-- .main-navigation -->
			<?php endif; ?>
		</div>
		
		<?php if ( has_nav_menu( 'social' ) ) : ?>
			<nav class="social-navigation d-none d-md-block" role="navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'social',
						'menu_class'     => 'social-links-menu d-flex justify-content-end',
						'depth'          => 1
					) );
				?>
			</nav><!-- .social-navigation -->
		<?php endif; ?>
	</footer><!-- .site-footer -->
	
	<div class="barra-creditos mx-md-auto">Copyrights © 2018 FMA Fine Martial Artists | Todos os direitos reservados <span class="d-none d-sm-block d-md-inline">| Desenvolvido por </span><span><img src="<?php echo get_template_directory_uri() ?>/img/mdmb.png" /></span></div>
<?php wp_footer(); ?>
</body>
</html>
