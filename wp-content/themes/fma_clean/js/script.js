$(document).ready(function(){
	$(".fma-post-slideshow").on("click", ".slideshow-nav a", function(e){
		e.preventDefault();
		
		var direction  = this.hash.replace("#","");
		var $slideshow = $(this).closest(".fma-post-slideshow").find("> ul");
		var $current = $slideshow.find(".current");
		var $next = $current.next("li");
		var $prev = $current.prev("li");
		var totalSlides = $slideshow.find("> li").length;
		var index = $current.index();
		
		$current.fadeTo(500,0, function(){
			$current.removeClass("current");
		})
		
		if(direction == "next"){
			if((index+1) == totalSlides)
				$slideshow.find("li").eq(0).fadeTo(500,1).addClass("current");
			else
				$next.fadeTo(500,1).addClass("current");
		}else{
			if(!index)
				$slideshow.find("li").eq(totalSlides-1).fadeTo(500,1).addClass("current");
			else
				$prev.fadeTo(500,1).addClass("current");
		}
	});
	
	setInterval(function(){
		//$(".fma-post-slideshow .slideshow-nav a").eq(1).click();
	},5000);
	
	$(".open-menu").click(function(e){
		e.preventDefault();
		$(".menu-mobile").toggleClass("opened");
		$(this).toggleClass("close-menu");
	});
	
	$(document).click(function(e){
		if(!$(e.target).parent().is(".open-menu") && !$(e.target).closest(".menu-mobile").length){
			$(".menu-mobile").removeClass("opened");
			$(".open-menu").toggleClass("close-menu");
		}
	});
	
	$(".autores ul").append("<li></li>");
	
	$(".autores").on("click", ".autores-nav a", function(e){
		e.preventDefault();
		
		var direction  = this.hash.replace("#","");
		
		if(direction == "next"){
			scrollTo = $(".autores ul li").eq(3).position().left;
		}else{
			scrollTo = $(".autores ul li").eq(0).position().left;
		}
		
		$(".autores .wrapper").animate({scrollLeft:scrollTo},500);
	});
	
	$(".es_textbox_class").attr("placeholder","E-MAIL");
});