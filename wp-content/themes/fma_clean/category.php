<?php get_header(); ?>

	<div class="d-flex justify-content-between conteudo mt-3">
		<div id="primary" class="content-area col-8 p-0 pr-3">
			<main id="main" class="site-main" role="main">
			<div class="section-fma">Posts em <?php echo get_the_category()[0]->name ?></div>

			<?php if ( have_posts() ) : ?>
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();
					$post_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
					
					$out  = "<a href='".get_the_permalink()."' >";
					$out .= "<div class='fma-blog-post short d-flex align-items-stretch'>";
					$out .= $post_image ? "<div class='thumbnail col-6 p-0 mr-3' style='background-image:url(".$post_image.")' /></div>" : "";
					$out .= "<div class='text d-flex flex-column justify-content-between'>";
					$out .= "<h2 class='title'>".get_the_title()."</h2>";
					$out .= "<div class='excerpt'>".get_the_excerpt()."</div>";
					$out .= "<div class='author'>Escrito por <b>".get_the_author()."</b></div>";
					$out .= "</div>";
					$out .= "</div>";
					$out .= "</a>";
					
					echo $out;
				endwhile;

				// Previous/next page navigation.
				the_posts_pagination();

			// If no content, include the "No posts found" template.
			else :
				echo "Nenhum post encontrado.";

			endif;
			?>

			</main><!-- .site-main -->
		</div><!-- .content-area -->
		
		<div class="col-4 p-0">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
