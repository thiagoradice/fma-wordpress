<?php get_header(); ?>

	<?php if(is_front_page()): 
		fma_posts_slideshow();
	?>
		<div class="fma-boxes-home d-flex justify-content-between align-items-stretch">
			<div class="autores">
				<h3>Colunistas do Blog</h3>
				<div class='autores-nav'>
					<a href='#prev'><span class='nav-button'><i class='fa fa-chevron-left'></i></span></a>
					<a href='#next'><span class='nav-button'><i class='fa fa-chevron-right'></i></span></a>
				</div>
				
				<div class="wrapper"><?php fma_get_authors(); ?></div>
			</div>
			<div class="newsletter">
				<h3>Receba nosso conteúdo em primeira mão!</h3>
				<?php es_subbox($namefield = "NO", $desc = "", $group = "Public"); ?>
			</div>
		</div>
	<?php endif; ?>
	
	<div class="d-flex justify-content-between mt-3 conteudo">
		<div id="primary" class="content-area col-8 p-0 pr-3">
			<main id="main" class="site-main" role="main">
			<div class="section-fma"><?php if($s){ ?>Resultado da pesquisa: <?php echo $s; }else{ ?>Últimas Postagens<?php } ?></div>

			<?php if ( have_posts() ) : ?>
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();
					$post_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
					
					$out  = "<a href='".get_the_permalink()."' >";
					$out .= "<div class='fma-blog-post short d-flex align-items-stretch'>";
					$out .= $post_image ? "<div class='thumbnail col-6 p-0 mr-3' style='background-image:url(".$post_image.")' /></div>" : "";
					$out .= "<div class='text d-flex flex-column justify-content-between'>";
					$out .= "<h2 class='title'>".get_the_title()."</h2>";
					$out .= "<div class='excerpt'>".get_the_excerpt()."</div>";
					$out .= "<div class='author'>Escrito por <b>".get_the_author()."</b></div>";
					$out .= "</div>";
					$out .= "</div>";
					$out .= "</a>";
					
					echo $out;
				endwhile;

				// Previous/next page navigation.
				the_posts_pagination();

			// If no content, include the "No posts found" template.
			else :
				echo "Nenhum post encontrado.";

			endif;
			?>

			</main><!-- .site-main -->
		</div><!-- .content-area -->
		
		<div class="col-4 p-0">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php get_footer(); ?>
