<?php

add_theme_support( 'custom-logo' );

function fma_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}

function register_my_menus() {
  register_nav_menus(
    array(
      'primary' => __( 'Primary' ),
      'assuntos' => __( 'Assuntos' ),
      'partners' => __( 'Parceiros' ),
      'social' => __( 'Social' )
    )
  );
}
add_action( 'init', 'register_my_menus' );


function theme_styles() {
	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'fonts_css', get_template_directory_uri() . '/css/fonts/fonts.css' );
	wp_enqueue_style( 'ptsans_css', 'https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700' );
	wp_enqueue_style( 'fontsawesome_css', 'https://use.fontawesome.com/releases/v5.0.10/css/all.css' );
	wp_enqueue_style( 'main_css', get_template_directory_uri() . '/css/style.css' );
}

add_action( 'wp_enqueue_scripts', 'theme_styles');

function js_scripts() {
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js');
	wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js');
}

add_action( 'wp_enqueue_scripts', 'js_scripts' );

$sidebar = array(
	'name'          => 'FMA Sidebar',
	'id'            => 'fma_sidebar',
	'description'   => '',
    'class'         => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h2 class="section-fma">',
	'after_title'   => '</h2>' );

register_sidebar( $sidebar );

add_theme_support('post-thumbnails', array(
	'post',
	'page',
	'loja',
));

add_theme_support( 'title-tag' );

function test_shortcodes()
{
    return 'Shortcodes are working!';
}
add_shortcode('test_shortcodes', 'test_shortcodes');
	
function fma_posts_slideshow($type = "post", $qtd = 5){
	$args = array(
		'posts_per_page'   => $qtd,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => $type,
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'author_name'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	);
	$posts_array = get_posts( $args );
	
	$ret = "<div class='fma-post-slideshow fma-".$type."'>";
	
	$ret .= "<div class='slideshow-nav'>";
	$ret .= "<a href='#prev'><span class='nav-button'><i class='fa fa-chevron-left'></i></span></a>";
	$ret .= "<a href='#next'><span class='nav-button'><i class='fa fa-chevron-right'></i></span></a>";
	$ret .= "</div>";
	
	$ret .= "<ul>";
	
	foreach($posts_array as $k => $post){
		$post_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
		$post_category = get_the_category();
		$current = !$k ? "current" : "";
		$cat = "";
		
		if($type == "post"){
			foreach($post_category as $category){
				$cat .= "<li>".$category->name."</li>";
			}
				
			$ret .= "<li style='background-image:url(".$post_image.")' class='".$current."'>";
			$ret .= "<div class='label'>";
			$ret .= $cat ? "<ul class='tags'>".$cat."</ul>" : "";
			$ret .= "<div class='title'>";
			$ret .= "<a href='".get_permalink()."'>".$post->post_title."</a>";
			$ret .= "</div>";
			$ret .= "</div>";
			$ret .= "</li>";
		}else{
			$ret .= "<li class='".$current."'>";
			$ret .= "<a href='".get_post_meta($post->ID, "Url", true)."'>";
			$ret .= "<img src='".$post_image."' />";
			$ret .= "<div class='title'>";
			$ret .= $post->post_title."<span class='preco'>".get_post_meta($post->ID, "Preço", true)."</span>";
			$ret .= "</div>";
			$ret .= "</a>";
			$ret .= "</li>";
		}
	}
	
	$ret .= "</ul>";
	$ret .= "</div>";
	
	echo $ret;
	//echo "<pre>", print_r($post), "</pre>";
}

function fma_get_authors() {
	global $wpdb;
	 
	//$authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users ORDER BY display_name");
	$authors = get_users('orderby=title&order=ASC&role=author');
	
	echo "<ul class='d-flex'>";
	 
	foreach($authors as $author) {
		echo "<li>";
		echo "<a href=\"".get_bloginfo('url')."/?author=";
		echo $author->ID;
		echo "\">";
		echo get_avatar($author->ID);
		echo '<div class="name">';
		the_author_meta('display_name', $author->ID);
		echo "</div>";
		echo "</a>";
		echo "</li>";
	}
	
	echo "</ul>";
}

// Register and load the widget
function wpb_load_widget() {
    register_widget( 'loja_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
 
// Creating the widget 
class loja_widget extends WP_Widget {
	 
	function __construct() {
		parent::__construct(
		 
		// Base ID of your widget
		'loja_widget', 
		 
		// Widget name will appear in UI
		__('Loja', 'loja_widget_domain'), 
		 
		// Widget description
		array( 'description' => __( 'Exibe post type Loja na sidebar', 'loja_widget_domain' ), ) 
		);
	}
 
	// Creating widget front-end
	 
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$qtd = apply_filters( 'widget_qtd', $instance['qtd'] );
		 
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		 
		// This is where you run the code and display the output
		fma_posts_slideshow("loja", $qtd);
		echo $args['after_widget'];
	}
				 
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'loja_widget_domain' );
		}
		if ( isset( $instance[ 'qtd' ] ) ) {
			$qtd = $instance[ 'qtd' ];
		}
		else {
			$qtd = __( '5', 'loja_widget_domain' );
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'qtd' ); ?>"><?php _e( 'Qtd Itens:' ); ?></label> 
			<input class="widefat" type="number" id="<?php echo $this->get_field_id( 'qtd' ); ?>" name="<?php echo $this->get_field_name( 'qtd' ); ?>" type="text" value="<?php echo esc_attr( $qtd ); ?>" />
		</p>
		<?php 
	}
		 
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['qtd'] = ( ! empty( $new_instance['qtd'] ) ) ? strip_tags( $new_instance['qtd'] ) : '';
		return $instance;
	}
} // Class loja_widget ends here